#!/usr/bin/env sh
#
if [ ! -d node_modules ]; then
    echo "Install nodejs..."
    cp -f package.json /src && npm cache clean --force && cd /src && npm install    
    cp -rf /src/node_modules $2
    echo $2
fi

if [ $3 == "prod" ]; then
	echo "In container: application dir is $2 and listen at $1"
	echo "Run prod..."
	echo "Build static assets..."
	yarn build
	echo "Serve assets..."
	serve -l $1 -s build
else
   	echo "In container: application dir is $2 and listen at $1"
	echo "Run dev..."
	echo "Set port to $1..."
    export PORT=$1
	echo "Serve assets and watch for changes..."
	nodemon --ignore db.json start
fi

